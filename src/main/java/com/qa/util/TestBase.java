package com.qa.util;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.List;
import java.util.Properties;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.interactions.Actions;


public class TestBase {
	public static WebDriver driver;
	public static Properties prop;
	public static Actions action;
	
	public TestBase()
	{
		try
		{
			prop=new Properties();
			FileInputStream fis=new FileInputStream("C:\\Release_Preview_2021.2\\BulkEmailInvoices_TDP_RP_2021_2\\src\\main\\java\\com\\qa\\config\\config.properties");
			prop.load(fis);
		}
		catch(IOException e)
		{
			e.getMessage();
		}
		
	}
	public static void initilizaton()
	{
		String browserName = prop.getProperty("browser");
		
		if(browserName.equals("chrome")){
		System.setProperty("webdriver.chrome.driver", "C:\\Selenium_Projects\\chrome_exe\\chromedriver_win32 (1)\\Chromedriver.exe");
		driver=new ChromeDriver();
		}
		driver.manage().window().maximize();
		driver.manage().deleteAllCookies();
		driver.manage().timeouts().implicitlyWait(TestUtil.IMPLICIT_WAIT,TimeUnit.SECONDS);
		driver.get(prop.getProperty("url"));
		
	}
	public void selectDropdownValue(WebElement dropdown, List<WebElement> dropdownList, String value) {
		action = new Actions(driver);
		action.moveToElement(dropdown).click().build().perform();
		for (int i = 0; i < dropdownList.size(); i++) {
			String formValue = dropdownList.get(i).getText().trim();
			if (formValue.equals(value)) {
				
				dropdownList.get(i).click();
				break;
			}
		}
	}

}
